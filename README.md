# loadTweets #

JavaScript library to load up to 20 latest tweets from Twitter user timeline without authentication.

### Usage ###

First, create a widget on your Twitter account [settings page](https://twitter.com/settings/widgets/new). Click *Create widget* and widget code will be generated. An 18-digit number in **data-widget-id** attribute is your widget id and it will be used later.

Library is provided as a Require.js module. It required jQuery and works asynchronously, returning jQuery Deferred object. You can use it like this:


```
#!javascript

require(['loadTweets'], function(loadTweets) {
	loadTweets('123456789012345678', 10).done(function(tweets) {
		console.log(tweets);
	});
});
```

Module provides the only function **loadTweets(widgetId, limit)**, where the first parameter is your widget id, the second is number of tweets to load. **limit** must be between 1 and 20.