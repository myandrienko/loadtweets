define(['jquery'], function($) {
	var cleanUp = function(container) {
		container.remove();
		$('#rufous-sandbox').remove();
		$('#twttrHubFrameSecure').remove();
		$('body').removeAttr('data-twttr-rendered');
	};

	var loadTweets = function(widgetId, limit) {
		// Hidden element to load widget into
		container = $('<div>').hide().appendTo('body');

		var loadingDeferred = new $.Deferred();

		// Load Twitter widgets-js utility
		$.getScript('https://platform.twitter.com/widgets.js')
		// Then create timeline inside a hidden element
		.then(function() {
			// Create a deferred object that resolves when timeline is loaded
			var timelineDeferred = new $.Deferred();
			twttr.ready(function() {
				twttr.events.bind('rendered', function(e) {
					timelineDeferred.resolve();
				});
			});

			twttr.widgets.createTimeline(widgetId, container.get()[0], {
				tweetLimit: limit
			});

			return $.when(timelineDeferred);
		}, function() {
			throw new Exception('Cannot load widgets-js from Twitter server');
		})
		// Parse timeline contents and extract tweets
		.then(function() {
			var tweets = [];
			container.find('iframe').contents().find('p.e-entry-title').each(function(i, tweet) {
				tweets.push($(tweet).html());
			});
			cleanUp(container);
			loadingDeferred.resolve(tweets);
		}, function() {
			throw new Exception('Failed to create Twitter timeline');
		});

		return $.when(loadingDeferred);
	};

	return loadTweets;
});